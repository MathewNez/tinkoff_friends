from rest_framework import serializers
from .models import *


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = '__all__'


class UserReadSerializer(serializers.ModelSerializer):
    interests = InterestSerializer(many=True, read_only=True)
    gender = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = User
        fields = '__all__'


class UserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class ContactReadSerializer(serializers.ModelSerializer):
    type = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Contact
        fields = '__all__'


class ContactWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all_'


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'
