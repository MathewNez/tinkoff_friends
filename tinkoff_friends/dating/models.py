from django.db import models


class Interest(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Gender(models.Model):
    name = models.CharField(max_length=7)

    def __str__(self):
        return self.name


class ContactType(models.Model):
    name = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class User(models.Model):
    nickname = models.CharField(max_length=32)
    first_name = models.CharField(max_length=32, null=True)
    last_name = models.CharField(max_length=32, null=True)
    birthday = models.DateField(null=True)
    interests = models.ManyToManyField(Interest)
    gender = models.ForeignKey(Gender, on_delete=models.SET_NULL, null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.first_name} {self.nickname} {self.last_name}'


class Contact(models.Model):
    type = models.ForeignKey(ContactType, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.CharField(max_length=32)

    def __str__(self):
        return self.value


class Image(models.Model):
    url = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.url
