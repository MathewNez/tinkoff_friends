from django.contrib import admin
from .models import *

admin.site.register(Interest)
admin.site.register(Gender)
admin.site.register(ContactType)
admin.site.register(City)
admin.site.register(User)
admin.site.register(Contact)
admin.site.register(Image)
