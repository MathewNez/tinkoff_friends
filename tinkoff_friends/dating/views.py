from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .models import Interest, User, City, Contact, Image
from .serializers import InterestSerializer, UserReadSerializer, UserWriteSerializer, \
    CitySerializer, ContactReadSerializer, ContactWriteSerializer, ImageSerializer


class InterestApiView(APIView):
    def get(self, request, *args, **kwargs):
        interests = Interest.objects.all()
        serializer = InterestSerializer(interests, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserDetailedApiView(APIView):
    def get_instance(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None

    def get(self, request, user_id, *args, **kwargs):
        instance = self.get_instance(user_id)
        if not instance:
            return Response({'error': 'User with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = UserReadSerializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, user_id, *args, **kwargs):
        instance = self.get_instance(user_id)
        if not instance:
            return Response({'error': 'User with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)
        serializer = UserWriteSerializer(instance=instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserApiView(APIView):
    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        serializer = UserReadSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = UserWriteSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CityDetailedApiView(APIView):
    def get(self, request, city_id, *args, **kwargs):
        try:
            serializer = CitySerializer(City.objects.get(id=city_id))
            return Response(serializer.data, status=status.HTTP_200_OK)
        except City.DoesNotExist:
            return Response({'error': 'City with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)


class CityApiView(APIView):
    def get(self, request, *args, **kwargs):
        cities = City.objects.all()
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ContactApiView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = ContactWriteSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ContactDetailedApiView(APIView):
    def get_instance(self, contact_id):
        try:
            return Contact.objects.get(id=contact_id)
        except Contact.DoesNotExist:
            return None

    def get(self, request, instance_id, *args, **kwargs):
        instances = Contact.objects.filter(user=instance_id)
        if not instances:
            return Response({'error': 'Contact with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = ContactReadSerializer(instances, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, instance_id, *args, **kwargs):
        instance = self.get_instance(instance_id)
        if not instance:
            return Response({'error': 'Contact with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = ContactWriteSerializer(instance=instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class ImageApiView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = ImageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ImageDetailedApiView(APIView):
    def get_instance(self, image_id):
        try:
            return Image.objects.get(id=image_id)
        except Image.DoesNotExist:
            return None

    def get(self, request, instance_id, *args, **kwargs):
        instances = Image.objects.filter(user=instance_id)
        if not instances:
            return Response({'error': 'No images for this user'}, status=status.HTTP_404_NOT_FOUND)

        serializer = ImageSerializer(instances, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, instance_id, *args, **kwargs):
        instance = self.get_instance(instance_id)
        if not instance:
            return Response({'error': 'Image with such id does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = ImageSerializer(instance=instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
